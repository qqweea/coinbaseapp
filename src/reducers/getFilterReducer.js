import * as TYPES from "../actions/actionTypes";

const initialState = {
  categories: {
    categoriesList: {},
    loading: true
  },
  coins: {
    coinsList: {},
    loading: true
  }
};

export default (state = initialState, action) => {
  switch (action.type) {
    case TYPES.GET_CATEGORIES:
      return {
        ...state,
        categories: {
          categoriesList: action.payload,
          loading: false
        },
        error: false
      };
    case TYPES.GET_COINS:
      return {
        ...state,
        coins: {
          coinsList: action.payload,
          loading: false
        },
        error: false
      };
    case TYPES.GET_FAILED:
      return {
        ...state,
        error: true
      };
    default:
      return state;
  }
};
