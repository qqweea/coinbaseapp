import * as TYPES from "../actions/actionTypes";

const initialState = {
  eventsList: {
    events: {},
    loading: true
  }
};

export default (state = initialState, action) => {
  switch (action.type) {
    case TYPES.GET_EVENTS:
      return {
        ...state,
        eventsList: {
          events: action.payload,
          loading: false
        },
        error: false
      };
    case TYPES.GET_FAILED:
      return {
        ...state,
        error: true
      };
    default:
      return state;
  }
};
