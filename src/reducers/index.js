import { combineReducers } from 'redux';
import filterReducer from './getFilterReducer';
import getEvents from './getEventsReducer'

export default combineReducers({
  getOptions: filterReducer,
  getEvents: getEvents,
});