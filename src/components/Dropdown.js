import React, { Component } from "react";
import { Select } from "antd";
const Option = Select.Option;

class Dropdown extends Component {
  state = {
    visibleItems: []
  };

  handleSearch = value => {
    const searchValue = value ? value.toLowerCase() : '';
    let match = [];
    let foundValues = [];
    if (value && value.length > 0) {
      match = this.state.visibleItems.filter(val =>
        val.name.toLowerCase().includes(searchValue)
      );
      if (match.length === 0) {
        foundValues = this.props.items.filter(item =>
          item.name.toLowerCase().includes(searchValue)
        );
      }
      this.setState({
        visibleItems: [...foundValues, ...this.state.visibleItems]
      });
    }
  };

  componentDidMount() {
    let {items} = this.props;
    items = items ? items : [];
    const slicedItems = items.length > 20
        ? items.slice(0, 20)
        : items;
    this.setState({ visibleItems: slicedItems });
  }

  render() {
    const { filterType, changeHandler } = this.props;
    const { visibleItems } = this.state;

    const options = visibleItems.map(item => (
      <Option key={item.id} value={item.id}>
        {item.name}
      </Option>
    ));
    return (
      <Select
        showSearch
        onSearch={this.handleSearch}
        mode="multiple"
        style={{ width: "100%" }}
        placeholder={`Select ${filterType}`}
        defaultValue={[]}
        onChange={(value) => changeHandler(value, filterType)}
        onBlur={this.props.focusHandler}
        allowClear
      >
        {options}
      </Select>
    );
  }
}

export default Dropdown;