import React, { Component } from "react";
import { connect } from "react-redux";
import { getFilterOptions } from "../actions/getFilterActions";
import { getEvents } from "../actions/getEventsActions";
import * as TYPES from "../actions/actionTypes";
import { Row, Col } from "antd";
import moment from "moment";

import Dropdown from "./Dropdown";
import { Button, DatePicker, Spin, Icon } from "antd";

const dateFormat = "DD/MM/YYYY";
const { RangePicker } = DatePicker;
const start = new Date();
const end = new Date(new Date().getFullYear(), 11, 31);

export class Categories extends Component {
  constructor(props) {
    super(props);
    const startDate = moment(start).format("DD/MM/YYYY");
    const endDate = moment(end).format("DD/MM/YYYY");

    this.state = {
      coins: [],
      categories: [],
      date: {
        start: startDate,
        end: endDate
      },
      page: 1,
      showPageNav: false
    };
  }

  getOptions = () => {
    this.props.getCategories();
    this.props.getCoins();
  };

  changeHandler = (value, filterType) => {
    this.setState({ [filterType]: value });
  };

  dateHandler = val => {
    const startDate = moment(val[0]).format("DD/MM/YYYY");
    const endDate = moment(val[1]).format("DD/MM/YYYY");
    this.setState({
      date: {
        start: startDate,
        end: endDate
      }
    });
  };

  applyFilter = () => {
    this.props.getEvents(this.state);
    this.setState({ page: 1 });
    // this.props.filterEvents(TYPES.FILTER_BY_COIN, this.state.coins);
    // this.props.filterEvents(TYPES.FILTER_BY_CATEGORY, this.state.categories);
  };

  changePage = val => {
    const newPage =
      val === "+"
        ? this.state.page + 1
        : this.state.page > 1
        ? this.state.page - 1
        : this.state.page;
    this.setState({ page: newPage });
    this.props.getEvents(this.state, newPage);
    window.scrollTo(0, 0);
  };

  componentDidMount() {
    this.getOptions();
  }
  render() {
    const { categories, coins } = this.props;
    return (
      <>
        <Row type="flex" gutter={30}>
          <Col xs={24} md={12} style={{ marginBottom: "10px" }}>
            {categories.loading ? (
              <Spin />
            ) : (
              <Dropdown
                items={this.props.categories.categoriesList}
                changeHandler={this.changeHandler}
                focusHandler={this.applyFilter}
                type={TYPES.FILTER_BY_CATEGORY}
                filterType="categories"
              />
            )}
          </Col>
          <Col xs={24} md={12} style={{ marginBottom: "10px" }}>
            {coins.loading ? (
              <Spin />
            ) : (
              <Dropdown
                items={this.props.coins.coinsList}
                changeHandler={this.changeHandler}
                focusHandler={this.applyFilter}
                type={TYPES.FILTER_BY_COIN}
                filterType="coins"
              />
            )}
          </Col>
        </Row>
        <Row type="flex" gutter={30}>
          <Col xs={24} style={{ marginBottom: "10px" }}>
            <RangePicker
              style={{ width: "100%" }}
              defaultValue={[
                moment(start, dateFormat),
                moment(end, dateFormat)
              ]}
              format={dateFormat}
              onChange={this.dateHandler}
              onLoad={this.dateHandler}
              onBlur={this.applyFilter}
            />
          </Col>
        </Row>
        <Row type="flex" gutter={30} style={{ justifyContent: "flex-end" }}>
          <Col style={{ marginBottom: "10px" }}>
            <Button
              type="primary"
              shape="circle"
              icon="search"
              onClick={this.applyFilter}
            />
          </Col>
        </Row>
        {this.props.children}
        
          <>
            <Row type="flex" style={{ justifyContent: "space-evenly" }}>
              <Button type="primary" onClick={() => this.changePage("-")}>
                <Icon type="left" />
                Backward
              </Button>
              <Button type="primary" onClick={() => this.changePage("+")}>
                Forward
                <Icon type="right" />
              </Button>
            </Row>
          </>
        
      </>
    );
  }
}

const mapStateToProps = state => ({
  categories: state.getOptions.categories,
  coins: state.getOptions.coins,
  filterEvents: state.filterEvents
});

const mapDispatchToProps = dispatch => {
  return {
    getCategories: () =>
      dispatch(getFilterOptions("categories", TYPES.GET_CATEGORIES)),
    getCoins: () => dispatch(getFilterOptions("coins", TYPES.GET_COINS)),
    getEvents: (state, page) => dispatch(getEvents(state, page))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Categories);
