import React, { Component } from "react";
import { connect } from "react-redux";
import { getEvents } from "../actions/getEventsActions";
import { Row, Col, Spin } from "antd";

import Event from "./EventItem";

export class Events extends Component {

  componentDidMount() {
    this.props.getEvents();
  }

  render() {
    const { events, loading } = this.props.eventsState.eventsList;
    const eventsCards = loading
      ? null
      : events.map(event => (
          <Col xs={24} md={12} lg={8} key={event.id}>
            <Event
              id={event.id}
              title={event.title}
              coins={event.coins}
              categories={event.categories}
              date={event.date_event}
            />
            <br />
          </Col>
        ));
    return (
      <>
        <Row type="flex" gutter={30}>
          {loading ? <Spin /> : eventsCards}
        </Row>
      </>
    );
  }
}

const mapStateToProps = state => ({
  eventsState: state.getEvents
});

const mapDispatchToProps = dispatch => {
  return {
    getEvents: () => dispatch(getEvents())
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Events);
