import React, { Component } from "react";
import { connect } from "react-redux";
import { Redirect, Link } from "react-router-dom";
import { Row, Col } from "antd";
import img from "../img/hot.png";

export class EventDetailed extends Component {
  render() {
    const { eventsState } = this.props;
    if (Array.isArray(eventsState)) {
      const ID = Number(this.props.params.match.params.id);
      const foundEvent = eventsState.filter(event => event.id === ID);
      if (foundEvent.length) {
        const [event] = foundEvent;
        const coinList = event.coins.map(coin => coin.name).join(", ");
        const categoriesList = event.categories
          .map(category => category.name)
          .join(", ");
        return (
          <div>
            <Link to="/">Go back</Link>
            {console.log(event)}
            <Row type="flex" justify="center">
              <Col>
                <h2>{event.title}</h2>
              </Col>
              {event.is_hot && (
                <Col>
                  <img src={img} alt="hot" style={{ maxHeight: "2em" }} />
                </Col>
              )}
            </Row>
            <Row type="flex" justify="center">
              <Col span={10}>
                <h4 style={{textAlign:"right"}}>{coinList}</h4>
              </Col>
              <Col>&nbsp;|&nbsp;</Col>
              <Col span={10}>
                <h4 style={{textAlign:"left"}}>{categoriesList}</h4>
              </Col>
            </Row>
            
            <Row>
              <Col md={{span: 12, push: 12}} sm={24}>
                <Row>
                  <Col>
                  <p>
                      <b>Confidence:</b>{" "}
                      {event.percentage} %
                    </p>
                  </Col>
                  <Col>
                    <p>
                      <b>When:</b>{" "}
                      {new Date(event.date_event).toLocaleDateString()}
                    </p>
                  </Col>
                  {event.can_occur_before && (
                    <Col>
                      <p>
                        <b>This event can occur before scheduled date!</b>
                      </p>
                    </Col>
                  )}
                  <Col>
                    <p>
                      <b>What:</b> {event.description}
                    </p>
                  </Col>
                  <Col>
                    <p>
                      <b>Source: </b> <a href={event.source} target="_blank" rel="noopener noreferrer" alt="source">source link</a>
                    </p>
                  </Col>
                </Row>
              </Col>
              <Col md={{span: 11, pull: 12}} sm={24}>
                <img src={event.proof} alt="proof" style={{maxWidth: "100%"}} />
              </Col>
            </Row>
          </div>
        );
      }
    }
    return <Redirect to="/" />;
  }
}

const mapStateToProps = state => ({
  eventsState: state.getEvents.eventsList.events
});

export default connect(mapStateToProps)(EventDetailed);
