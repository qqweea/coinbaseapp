import React from "react";
import { Card, Row, Col } from "antd";
import moment from "moment";
import {Link} from 'react-router-dom'
import icon from './genericIcon.svg'

const dateFormat = "DD/MM/YYYY";

export default function Event(props) {
  const { id, title, coins, categories, date } = props;
  const coinList = coins.map(coin => coin.name).join(", ");
  const categoriesList = categories.map(category => category.name).join(", ");
  const formattedDate = moment(date).format(dateFormat);

  return (
    <Card 
    hoverable
    
    title={title} extra={<Link to={`event/${id}`}>More info</Link>} style={{ width: "100%" }}>
      <Row gutter={16}>
        <Col span={6}>
        {/* <i className={`cc ${coins[0].symbol}`}></i> */}
        <img alt="icon" src={icon}  style={{width: "100%"}}/>
        </Col>
        <Col span={18}>
        <h3>{coinList}</h3>
        <h4>{categoriesList}</h4>
        <p>{formattedDate}</p>
        
        </Col>
      </Row>
      
    </Card>
  );
}
