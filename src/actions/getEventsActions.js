import * as TYPES from "./actionTypes";
import { BASE_URL, GET_REQUEST_TOKEN } from "./actionParms";
import axios from "axios";

export const getEvents = (params = null, page = 1) => dispatch => {
  const coins = params !== null ? params.coins.join() : "";
  const categories = params !== null ? params.categories.join() : "";
  const startDate = params !== null ? params.date.start : "";
  const endDate = params !== null ? params.date.end : "";
  const GET_REQUEST_PARAMS = {
    params: {
      access_token: GET_REQUEST_TOKEN,
      page: page,
      max: 16,
      coins: coins,
      categories: categories,
      dateRangeStart: startDate,
      dateRangeEnd: endDate
    }
  };
  const eventsGetRequest = axios.get(`${BASE_URL}events`, GET_REQUEST_PARAMS);
  eventsGetRequest
    .then(events => {
      dispatch({
        type: TYPES.GET_EVENTS,
        payload: events.data
      });
    })
    .catch(err =>
      dispatch({
        type: TYPES.GET_FAILED
      })
    );
};
