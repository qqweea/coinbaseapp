import { GET_FAILED } from "./actionTypes";
import { BASE_URL, GET_REQUEST_TOKEN } from "./actionParms";
import axios from "axios";

export const getFilterOptions = (getOptions, getType,  params = {}) => dispatch => {
  const GET_REQUEST_PARAMS = {
    params: {
      access_token: GET_REQUEST_TOKEN,
      ...params
    }
  };
  const optionsGET = axios.get(
    `${BASE_URL}${getOptions}`,
    GET_REQUEST_PARAMS
  );
  optionsGET
    .then(options =>
      dispatch({
        type: getType,
        payload: options.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_FAILED
      })
    );
};
