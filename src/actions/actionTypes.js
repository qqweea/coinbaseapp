export const GET_CATEGORIES = 'GET_CATEGORIES';
export const GET_COINS = 'GET_COINS';
export const GET_FAILED = 'GET_FAILED';
export const FILTER_BY_CATEGORY = 'FILTER_BY_CATEGORY';
export const FILTER_BY_COIN = 'FILTER_BY_COIN';
export const GET_EVENTS = 'GET_EVENTS';
export const GET_EVENT_BY_ID = 'GET_EVENT_BY_ID';