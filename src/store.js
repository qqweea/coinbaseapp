import { createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import rootReducer from "./reducers";
import { composeWithDevTools } from 'redux-devtools-extension';
import localStorage from 'redux-persist/lib/storage'
import { persistStore, persistReducer } from 'redux-persist'

const initialState = {};

const middleware = [thunk];

const persistConfig = {
  key: 'root',
  storage: localStorage,
}
 
const persistedReducer = persistReducer(persistConfig, rootReducer)

export const store = createStore(persistedReducer, initialState, composeWithDevTools(
  applyMiddleware(...middleware),
));

export const persistor = persistStore(store)
