import React, { Component } from "react";
import { Route } from "react-router-dom";
import "./App.css";

import Filter from "./components/Filter";
import Events from "./components/Events";
import EventDetailed from './components/EventDetailed'



class App extends Component {
  render() {
    return (
      <div className="App container">
        <Route
          path="/"
          exact
          render={() => (
            <Filter>
              <Events />
            </Filter>
          )}
        />
        <Route path="/event/:id" component={(match)=><EventDetailed params={match}/>} />
      </div>
    );
  }
}

export default App;
